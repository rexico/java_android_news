package com.example.news.dummy;

import android.util.Log;

import com.example.news.business.dto.PublicationDTO;
import com.example.news.database.entity.Publication;

import java.util.ArrayList;
import java.util.List;

public class DummyContent {

    public static final List<PublicationDTO> ITEMS = new ArrayList<>();
    public static final List<Publication> ENTITYS = new ArrayList<>();

    static {
        try{
            ENTITYS.addAll(Publication.listAll(Publication.class));
        } catch (Exception  e) {
            e.printStackTrace();
            Log.e("LE", "Exception: " + e);

        }

    }
}