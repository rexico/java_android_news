package com.example.news;

import androidx.recyclerview.widget.RecyclerView;

import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.news.business.dto.PublicationDTO;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * {@link RecyclerView.Adapter} that can display a {@link PublicationDTO}.
 * TODO: Replace the implementation with code for your data type.
 */
public class MyPublicationRecyclerViewAdapter extends RecyclerView.Adapter<MyPublicationRecyclerViewAdapter.ViewHolder> {

    public interface OnPublicationRecyclerViewAdapterListener {
        void onItemClick(PublicationDTO item);
    }

    private final List<PublicationDTO> mValues;
    private OnPublicationRecyclerViewAdapterListener listener;

    public MyPublicationRecyclerViewAdapter(OnPublicationRecyclerViewAdapterListener listener, List<PublicationDTO> items) {
        mValues = items;
        this.listener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_publication, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = mValues.get(position);
        Picasso.get().load(Uri.parse(mValues.get(position).getImageUrl())).into(holder.imageView);
        holder.tvTitle.setText(mValues.get(position).getTitle());
        holder.tvDescription.setText(mValues.get(position).getDescription());
        holder.tvPubDate.setText(mValues.get(position).getPubDate());
        holder.tvAuthors.setText(mValues.get(position).getAuthors().toString());

        holder.mView.setOnClickListener(v -> {
            listener.onItemClick(mValues.get(position));
        });
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;

        public final ImageView imageView;
        public final TextView tvTitle, tvDescription, tvPubDate, tvAuthors;

        public PublicationDTO mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;

            imageView = view.findViewById(R.id.imageView);
            tvTitle = view.findViewById(R.id.tvTitle);
            tvDescription = view.findViewById(R.id.tvDescription);
            tvPubDate = view.findViewById(R.id.tvPubDate);
            tvAuthors = view.findViewById(R.id.tvAuthors);
        }

    }
}