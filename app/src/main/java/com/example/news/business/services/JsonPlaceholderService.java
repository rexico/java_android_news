package com.example.news.business.services;

import com.example.news.business.dto.ResponseDTO;


import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;

public interface JsonPlaceholderService {
    @Headers("x-api-key: 0jhljhy3gslbkaa8wppc7c0un")
    @GET("v1/news")
    Call<ResponseDTO> getAllNews();
}
