package com.example.news;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.example.news.business.dto.PublicationDTO;
import com.example.news.business.dto.ResponseDTO;
import com.example.news.business.services.JsonPlaceholderService;
import com.example.news.database.entity.Publication;
import com.example.news.dummy.DummyContent;
import com.orm.SugarContext;

import java.util.Collections;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity
        implements ViewPublicationFragment.OnViewPublicationFragmentListener
        , PublicationFragment.OnPublicationFragmentListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SugarContext.init(this);
        setContentView(R.layout.activity_main);
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://api.datanews.io/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        JsonPlaceholderService jsonPlaceholderService = retrofit.create(JsonPlaceholderService.class);

        jsonPlaceholderService.getAllNews().enqueue(new Callback<ResponseDTO>() {
            @Override
            public void onResponse(Call<ResponseDTO> call, Response<ResponseDTO> response) {
                ResponseDTO body = response.body();
                List<PublicationDTO> news = body.getHits();
                if(DummyContent.ENTITYS.size() != 0) {
                    for(PublicationDTO p : news){
                        try {
                            Publication e = Publication.find(Publication.class, "title=?", p.getTitle()).get(0);
                                p.setViewing(e.getViewing());
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                }
                Collections.sort(news);
                DummyContent.ITEMS.addAll(news);
                PublicationFragment fr = (PublicationFragment)getSupportFragmentManager().findFragmentById(R.id.fragment_container_view);
                fr.adapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(Call<ResponseDTO> call, Throwable t) {

            }
        });

        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.fragment_container_view, new PublicationFragment(), "LIST_TAG")
                .commit();
    }

    @Override
    public void onBtnBackClick() {
        Collections.sort(DummyContent.ITEMS);
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.fragment_container_view, new PublicationFragment(), "LIST_TAG")
                .commit();
    }

    @Override
    public void onItemSelect(PublicationDTO item) {
        ViewPublicationFragment f = new ViewPublicationFragment();
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.fragment_container_view, f, "INFO_TAG")
                .commit();
        f.setData(item);
    }
}